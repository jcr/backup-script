# Backup - Version 2.1.1

Tool for backup and synchronization of folders.

Directories/files are copied recursively. Links, owner, groups, permissions and times modifications are preserved.

Backup command is based on rsync tool.

It's equivalent to ``` rsync -av --progress [--delete]```

## Requirements

* Debian 9+
* Rsync

## Usage

```
backup.sh -o [Origin folder] -d [Destination folder]
```

OPTIONS:
-h      Display help
-o      Origin folder
-d      Destination folder

### Directories declaration

This would recursively transfer bar folder and all its files from the directory /origin/foo/bar into the /destination/tmp/ directory :

```
backup.sh -o /origin/foo/bar -d /destination/tmp/
```

A trailing slash on the source changes this behavior to avoid creating an additional directory level at the destination.

You can think of a trailing / on a source as meaning "copy the contents of this directory" as opposed to "copy the directory by name".

In this case, bar folder won't be created in /destination/tmp directory but all bar's files will be copy into tmp folder.

```
backup.sh -o /origin/foo/bar/ -d /destination/tmp
```

But in both cases the attributes of the containing directory are transferred to the containing directory on the destination.

In other words, each of the following commands copies the files in the same way, including their setting of the attributes of /dest/foo:

```
backup.sh -o /origin/foo -d /dest
backup.sh -o /origin/foo/ -d /dest/foo
```

### Deletion

By default, Backup.sh deletes in destination directory all folders and files which aren't present in the origin directory. Fortunately, a check is realized before deletion. It able you to see which files will be deleted during synchronization and to confirm this before.

If you absolutely don't want to delete any file in destination directory, you can bypass the check/deletion steps thanks to -c option. Backup will be realized without any deletion.

```
backup.sh -c -o /origin/foo/ -d /dest/foo
```

## Credits

Tool developped by Jonas Chopin-Revel on Licence GNU-GPLv3.

Gitlab : https://framagit.org/jcr/backup-script

**Share and improve it. It's free !**


A part of this documentation and this script is based on rsync.

Rsync - GNU-GPLv3 - https://rsync.samba.org/

Thanks to them for this super tool !
