#!/bin/bash
set -e

#############
# Functions #
#############

#Manual

usage() {
cat << EOF

################
# Backup 2.1.1 #
################

Tool for backup and synchronization of folders.
Directories are copied recursively. Links, owner, groups, permissions and times modifications are preserved.
By default, deletion mode is enabled. Files which don't exist in origin folder will be display and delete in destination folder if confirm deletion.
If you prefer to backup directly without dry-run and deletion steps, set -c option to enable conservative mode.

Usage: backup.sh [-c] -o [Origin folder] -d [Destination folder]

OPTIONS:
    -h      Display help
    -o      Origin folder
    -d      Destination folder
    -c      Conservative mode. Copy without to delete files in destination folder which don't exist in origin folder

Tool developped by Jonas Chopin-Revel on Licence GNU-GPLv3.
Share and improve it. It's free !

EOF
}

# Test if destination folder exists and create it if it's not

test_destination_folder () {
    read -p "Destination folder doesn't exist. Do you want to create it ? yes/no : " CREATE_FOLDER
        if [ "$CREATE_FOLDER" = "yes" ]
        then
            mkdir "$DESTINATION_FOLDER" && rsync -av --progress "$ORIGIN_FOLDER" "$DESTINATION_FOLDER" && sync
        elif [ "$CREATE_FOLDER" = "no" ]
        then
            until [ -e "$DESTINATION_FOLDER" ]
            do
                read -p "Destination folder doesn't exist. Try again : " DESTINATION_FOLDER
            done
            synchronization
        else
            echo "C'mon ! Be precise !!!"
            RETURN_CODE='1'
        fi
}

# Synchronization of files with or without deletion in the destination directory

synchronization () {
    if [ "$CONSERVATIVE_MODE" -ne 1 ]
    then
        DELETION_TEST=$(rsync -av --progress --delete --dry-run "$ORIGIN_FOLDER" "$DESTINATION_FOLDER"|grep "^deleting "|awk -F " " '{for(i=2; i<=NF; ++i) printf "%s ", $i; print ""}'|awk '{printf("%s\\n"), $i}')

        if [ -n "$DELETION_TEST" ]
        then
            echo -e "\nThese files will be deleted or moved :\n"
            echo -e "$DELETION_TEST"
            until [[ "$DELETION_PERMISSION" = "yes" ]] || [[ "$DELETION_PERMISSION" = "no" ]]
            do
                read -p "Do you agree to delete these files and folders ? yes/no " DELETION_PERMISSION
            done

            if [ "$DELETION_PERMISSION" = "yes" ]
            then
                rsync -av --progress --delete "$ORIGIN_FOLDER" "$DESTINATION_FOLDER" && sync
                RETURN_CODE='0'
            elif [ "$DELETION_PERMISSION" = "no" ]
            then
                printf "\nSynchronization cancelled !\nWe suggest you to run with -c option to avoid files deletion.\n"
                RETURN_CODE='1'
            fi
        else
            rsync -av --progress --delete "$ORIGIN_FOLDER" "$DESTINATION_FOLDER" && sync
            RETURN_CODE='0'
        fi
    else
        rsync -av --progress "$ORIGIN_FOLDER" "$DESTINATION_FOLDER" && sync
        RETURN_CODE='0'
    fi
}

#############
# Variables #
#############

ORIGIN_FOLDER=''
BAD_OPTION=''
DESTINATION_FOLDER=''
CONSERVATIVE_MODE='0'
RETURN_CODE=''

# Retrive parameters

while getopts "cho:d:" OPTION
do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        o)
            ORIGIN_FOLDER=$OPTARG
            ;;
        d)
            DESTINATION_FOLDER=$OPTARG
            ;;
        c)
            CONSERVATIVE_MODE='1'
            ;;
        \?)
            BAD_OPTION='1'
            RETURN_CODE='1'
            ;;
     esac
done

########
# Core #
########

if [[ $BAD_OPTION -ne 1 ]]
then
    if [ -e "$ORIGIN_FOLDER" ] && [ -e "$DESTINATION_FOLDER" ] && [ "$ORIGIN_FOLDER" != "$DESTINATION_FOLDER" ]
    then
        synchronization

    elif [ ! -e "$ORIGIN_FOLDER" ]
    then
        until [ -e "$ORIGIN_FOLDER" ]
        do
            read -p "Origin folder doesn't exist. Try again : " ORIGIN_FOLDER
        done
        if [ "$?" -eq 0 ]
        then
            test_destination_folder
        fi

    elif [ ! -e "$DESTINATION_FOLDER" ]
    then
        test_destination_folder
    fi
    exit $RETURN_CODE
else
    echo "### Invalid option. ###"
    usage
    exit $RETURN_CODE
fi
